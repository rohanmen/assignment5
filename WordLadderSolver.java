
import java.util.*;

// do not change class name or interface it implements
public class WordLadderSolver implements Assignment5Interface
{
    List<String> SolutionList;
    Dictionary dic = new Dictionary(Assign5Driver.dicFileName);

    // add a constructor for this object. HINT: it would be a good idea to set up the dictionary there

    // do not change signature of the method implemented from the interface
    @Override
    public List<String> computeLadder(String startWord, String endWord) throws NoSuchLadderException {
        SolutionList  = new ArrayList<String>();
    	boolean check = makeLadder(startWord, endWord, 0);
    	if(check == false)
    	{
    		SolutionList.remove(startWord);
    		throw new NoSuchLadderException("No ladder exists.");
    	}
    	
        //throw new UnsupportedOperationException("Not implemented yet!");
		return SolutionList;
    }

    @Override
    public boolean validateResult(String startWord, String endWord, List<String> wordLadder) {
    	if(startWord.equals(endWord)){return true;}
    	boolean result = true;
    	int k;
    	int count = 0;
    	int i = 1;
    		while(result && i < wordLadder.size())
    		{
    			k = 0;
    			count = 0;
    			while(k<wordLadder.get(i).length())
    			{
    				if(wordLadder.get(i).charAt(k) != wordLadder.get(i-1).charAt(k))
    				{
    					count += 1;
    				}
    				k+=1;
    			}
    			if(count!=1)
    			{
    				result = false;
    			}
    			i+=1;
    		}
		return result;
        //throw new UnsupportedOperationException("Not implemented yet!");
    }

    /**
     * Makes the word ladder using the fromWord and the toWord
     * @param fromWord the beginning word of the word ladder
     * @param toWord the ending word of the wordLadder
     * @param index of the fromWord where the comparsion is being made
     * @return true if the fromWord equals toWord else returns false
     */
    public boolean makeLadder(String fromWord, String toWord, int index)
    {
    	SolutionList.add(fromWord);
    	if(fromWord.equals(toWord)) 
    	{
    		if(SolutionList.size() == 1){SolutionList.add(toWord);}
    		return true;
    	}
    	boolean result = true;
    	int count = 0;
    	int newIndex = index;
    	List<String> candidateWords = new ArrayList<String>();
    	while(count < 4)
    	{
	    	if(newIndex < fromWord.length() - 1){newIndex += 1;}
	    	else {newIndex =0;}
	    	candidateWords.addAll(dic.searchDictionary(fromWord, SolutionList, newIndex));
	    	count+=1;
    	}
    	if(candidateWords.isEmpty())
    	{
    		result = false;
    	}
    	
    	boolean latterFound = false;
    	String temp = "";
    	if(result)
    	{
    	candidateWords = compare(toWord, candidateWords);
    		int j = 0;
    		while((latterFound == false) && (j < candidateWords.size()))
    		{
    			temp = candidateWords.get(j).substring(1, 6);
    			int k = 0;
    			while(temp.charAt(k) == fromWord.charAt(k))
    			{
    				k += 1;
    			}
    			if(!SolutionList.contains(temp))
    			{
    				latterFound = makeLadder(temp, toWord, k);
    			}
    			j+=1;	    			
    		}
    	}
    	
    	
    	if(latterFound){return true;}
    	else
    	{
    		if(toWord.equals("buddy"))
    		{
    		
    		}
    		if(!temp.equals(""))
    		{
    			SolutionList.remove(temp);
    		}
    		else 
    		{
    			SolutionList.remove(fromWord);
    		}
    		return false; 
    	}
    }
    
    /**
     * Finds a possible list of words with one letter difference from toWord
     * @param toWord the word that the candidate list is based of.  the list contains words one letter away from toWord
     * @param candidate a list of words one letter away from toWord
     * @return a list containing words one letter away from toWord
     */
    public static List<String> compare(String toWord, List<String> candidate)
	{
		Iterator<String> i = candidate.iterator();
		List<String> numCandidate = new ArrayList<String>();
		while(i.hasNext())
		{
			int count = 0;
			String toCompare = i.next();
			for(int x = 0; x < 5; x++)
			{
				if(toWord.charAt(x) == toCompare.charAt(x))
				{
					count++;
				}
			}
			Integer sum = new Integer(count);
			String number = sum.toString();
			number = number + toCompare;
			numCandidate.add(number);
		}
		Collections.sort(numCandidate);
		Collections.reverse(numCandidate);
		return numCandidate;
	}
    
    
//    public ArrayList<String> prepend(ArrayList<String> candidateWords, String toWord)
//    {
//    	ArrayList<String> prepended = new ArrayList<String>();
//    	for(int i = 0; i < candidateWords.size(); i += 1)
//    	{
//    		prepended.add(compare(toWord, candidateWords.get(i)) + candidateWords.get(i));
//    	}
//    	return Collections.sort(prepended);
//    }
}

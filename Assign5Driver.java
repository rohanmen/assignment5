
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class Purpose:
 * @author Rohan M., Chris Hogue, Jerome Vernon
 *
 */
public class Assign5Driver
{
	private static ArrayList<String> begWord;
	private static ArrayList<String> endWord;
	final static String fileName = "assn5data.txt";
	final static String dicFileName = "assn5words.dat";
	
    public static void main(String[] args)
    {
    	
    	Dictionary myDict = new Dictionary(dicFileName);
    	
    	
        // Create a word ladder solver object
        Assignment5Interface wordLadderSolver = new WordLadderSolver();

        processFile(fileName);
//        // Read file in
//        // for each line in the file call compute ladder
//        try {
//            List<String> result = wordLadderSolver.computeLadder("money", "honey");
//            boolean correct = wordLadderSolver.validateResult("money", "honey", result);
//        } catch (NoSuchLadderException e) {
//            e.printStackTrace();
//        }
        
        StopWatch s = new StopWatch();
	    long mytime;
	    s.start();
        
        for(int i = 0; i < begWord.size(); i++)
        {
        	try
        	{
        		//check for valid words
        		if(begWord.get(i).length() != 5 || !myDict.isValid(begWord.get(i)))
        		{
        			System.out.println(begWord.get(i) + " is not legitimate 5-letter word from the dictionary");
        			System.out.println("**********");
        			continue;
        		}
        		else if(endWord.get(i).length() !=5 || !myDict.isValid(begWord.get(i)))
        		{
        			System.out.println(endWord.get(i) + " is not a legitimate 5-letter word from the dictionary");
        			System.out.println("**********");
        			continue;
        		}
        	
        		
        		List<String>result = wordLadderSolver.computeLadder(begWord.get(i), endWord.get(i));
        		boolean correct = wordLadderSolver.validateResult(begWord.get(i), endWord.get(i), result);
        		
        		if(correct && !result.isEmpty() && (result.size() != 1))
        		{
        			printWordLadder(result);
        		}
        		else
        		{
        			System.out.println("There is no word ladder between " + begWord.get(i) + " and " + endWord.get(i));
        		}
        	}
        	catch (NoSuchLadderException e)
        	{
        		System.out.println(e.toString());
    			System.out.println("There is no word ladder between " + begWord.get(i) + " and " + endWord.get(i));

        	}
        	
        	System.out.println("**********");
        }
        
        s.stop();
	    mytime=s.getElapsedTime();
	    System.out.println("Time elapsed in nanoseconds is: "+ mytime);
	    System.out.println("Time elapsed in seconds is: "+ mytime/ StopWatch.NANOS_PER_SEC);
        
    }
    
    /**
     * Opens and reads the file and stores the words into two parallel lists containing the starting and the ending word
     * @param fileName the name of the file that is to be opened and read
     */
    public static void processFile(String fileName)
    {
    	begWord = new ArrayList<>();
    	endWord = new ArrayList<>();
    	
		try 
		{
			FileReader freader = new FileReader(fileName);
			BufferedReader reader = new BufferedReader(freader);
			
			for (String s = reader.readLine(); s != null; s = reader.readLine()) 
			{
				//System.out.println("The input String is: " + s);
				
				//remove spaces int the begenning of the word
				while(s.substring(0, 1).equals(" "))
				{
					s = s.substring(1);
				}
				
				//get first word
				int i = 0;
				String word = "";
				while(!(s.substring(i, i + 1).equals(" ")))
				{
					word = word + s.substring(i , i+1);
					i++;
				}
				begWord.add(word);
				
				s = s.substring(i);

				s = s.replaceAll("\\s+","");
				endWord.add(s);
				
				//System.out.println(begWord.get(begWord.size() - 1) + " " + endWord.get(endWord.size() - 1));
		
			}
			reader.close();
		} 
		catch (FileNotFoundException e) 
		{
			System.err.println ("Error: File not found. Exiting...");
			e.printStackTrace();
			System.exit(-1);
		} 
                catch (IOException e) 
		{
			System.err.println ("Error: IO exception. Exiting...");
			e.printStackTrace();
			System.exit(-1);
		}
    	
    }
    
    /**
     * Prints the wordLadder that is passed in as a List to the console
     * @param ladder the name of the List that is to be printed to the console
     */
    public static void printWordLadder(List<String> ladder)
    {
    	for(int i = 0; i < ladder.size(); i++)
    	{
    		System.out.println(ladder.get(i));
    	}
    }
}
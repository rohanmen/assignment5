
//Jerome Vernon 
//Hours worked:		12 hours 0 minutes
//Logic errors: 	1


import java.io.File; 
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

public class Dictionary 
{
	//Class members
	private static LinkedHashMap<String, Integer> words = new LinkedHashMap<String, Integer>();	//"The dictionary"
	//Constructor
	/**
	 * Constructor for the Dictionary Class
	 * @author Jerome Vernon
	 * @param fileName
	 */
	public Dictionary(String fileName)
	{
		Scanner sc = null;
		try
		{
			File file = new File(fileName);
			sc = new Scanner(file);
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
		while(sc.hasNext())
		{
			String word = new String();
			String line = sc.nextLine();
			if(line.charAt(0) != '*')
			{
				word = line.substring(0, 5);
				words.put(word, word.hashCode());
			}
		}
		words.toString();
		sc.close();
	}
	//Class functions
	
	/**
	 * Returns all possible words to change to within one index.  
	 * @author 	Jerome Vernon
	 * @param 	word - the word that we are looking to change by one letter.
	 * @param 	num - the position that is going to be changed. Range 0 <= num <= 4
	 * @return 	a list of all the words that one letter different for the first position such list exists.
	 * 			returns null if list of words cannot be found for any letter position.
	 */
	public List<String> searchDictionary(String word, List<String> solutionList, int num)
	{
		List<String> candidate = new ArrayList<String>();
		StringBuilder possible = new StringBuilder(word);
		for(char i = 'a'; i <= 'z'; i++)
		{
			Character x = new Character(i);
			possible.replace(num, num+1, x.toString());
			if(!(possible.toString().equals(word)))
			{
				if (this.isValid(possible.toString()))
				{
					candidate.add(possible.toString());
				}
			}
		}
		//Delete words in solution list here
		Iterator<String> ci = candidate.iterator();
		//Iterator<String> sLi = solutionList.iterator();
		while(ci.hasNext())
		{
			String choice = ci.next();
			if (solutionList.contains(choice))
			{
				ci.remove();
			}
		}	
		if(candidate.size() != 0)
		{
			return candidate;		
		}
		return candidate;
	}
	
	/**
	 * Determins whether or not a word is valid
	 * @author Jerome Vernon
	 * @param word - a String that is a potential word
	 * @return - boolean - true if word exists in dictionary - false if not in dictionary.
	 */
	public boolean isValid(String word)
	{
		if (words.containsKey(word))
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	/**
	 * @author Jerome Vernon
	 * @return A string with the words and their corresponding hashcodes
	 * This method return a string representation of the HashTable that is used for the Dictionary
	 * USED FOR DEBUGGING PURPOSES
	 */
	public String toString()
	{
		return words.toString();
	}
}
